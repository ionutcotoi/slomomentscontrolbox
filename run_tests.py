#!/usr/bin/env python
import sys
import serial
import time

#stepper_values = [500, 1000, 2000,  4000, 8000, 9000, 10000, \
#5000, 2000, 100, 50, 25, 10, 5, 0, -5000, -5000, -5000, 0]
# stepper_values = [i for i in range(0,5000,50)]
stepper_values = [0,100,200,300,400,400,400,400,400,800,1200,1000, 500,0]
# stepper_values = [0,100,100,100,200,100,0]

focus_values = [i for i in range(0,10001,500)]
def main(*args):
    serial_port = sys.argv[1]
    intf_serial_port = None
    device = "S0"
    fps = 5
    #frames = len(stepper_values)
    frames = len(stepper_values)
    cmd = ""

    if len(args[0]) > 1:
        serial_port = args[0][1]

    print "Serial Port:", serial_port
    intf_serial_port = serial.Serial(port=serial_port, baudrate=115200, timeout=10)
    msg = ""
    while intf_serial_port.inWaiting() > 0:
        msg += intf_serial_port.read(1).decode("ascii")

    print(msg)
    cmd = "BFPS "+str(fps)+" "+str(frames)+"\n"
    print cmd
    nrbytes = intf_serial_port.write(cmd)
    intf_serial_port.flush()

    while intf_serial_port.inWaiting() > 0:
        msg += intf_serial_port.read(1).decode("ascii")

    print(msg)

    for i in range(1, frames, 1):
        cmd = "STOR "+device+" "+str(i)+" "+str(stepper_values[i])+"\n"
        print cmd
        nrbytes = intf_serial_port.write(cmd)
        intf_serial_port.flush()
        while intf_serial_port.inWaiting() > 0:
            msg += intf_serial_port.read(1).decode("ascii")

        print(msg)

    cmd = "SHOW\n"
    intf_serial_port.write(cmd)

    while intf_serial_port.inWaiting() > 0:
        msg += intf_serial_port.read(1).decode("ascii")

    print(msg)

    cmd = "INIT\n"
    intf_serial_port.write(cmd)

    while intf_serial_port.inWaiting() > 0:
        msg += intf_serial_port.read(1).decode("ascii")

    print(msg)
    time.sleep(2)
    intf_serial_port.write("RRUN\n")

if __name__ == "__main__":
    main(sys.argv)
