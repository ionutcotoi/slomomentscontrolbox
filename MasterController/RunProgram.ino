int sign(double x){
  if (x>0) return 1;
  return -1;
}


// Run the stored points in EEPROM according to the fps 
void runProgram() {
  
    //SerialCtrl.println("Thread: running...");
    if (S.frame_pointer > S.n_frames-1){
      
      S.frame_pointer = 0;
      SerialCtrl.println("program finished");
      runProgThread.enabled = false;
      stepperInterpolationThread.enabled=false;
      stepper0.moveTo(S.S0[S.n_frames-1]);
      stepper1.moveTo(S.S1[S.n_frames-1]);
    }
    
    S.frame_pointer++;
    
    
    // digitalPotWrite(F0_CSS,S.F0[S.frame_pointer]);
    // digitalPotWrite(F1_CSS,S.F1[S.frame_pointer]);
    Serial2.print("F0 ");
    Serial2.println(S.F0[S.frame_pointer]);
    Serial2.print("F1 ");
    Serial2.println(S.F1[S.frame_pointer]);
    // float S0_spd1, S1_spd1;
    // if(S.frame_pointer==1){
    //   S0_spd1 = 0;
    //   S1_spd1 = 0;
    // }
    // else{
    //   S0_spd1 = (S.S0[S.frame_pointer-1]-S.S0[S.frame_pointer-2])*S.fps;
    //   S1_spd1 = (S.S1[S.frame_pointer-1]-S.S1[S.frame_pointer-2])*S.fps;
    // }
    // float S0_spd0 = (S.S0[S.frame_pointer]-S.S0[S.frame_pointer-1])*S.fps;
    // float S1_spd0 = (S.S1[S.frame_pointer]-S.S1[S.frame_pointer-1])*S.fps;
    //stepper0.setMaxSpeed(min(S0_spd0, S0_MAX_SPD));
    //stepper1.setMaxSpeed(min(S1_spd0, S1_MAX_SPD));
    /****************************************************
     x(t)= x0 + v*t + 1/2*a*t^2 + 1/6*a'*t^3
    ****************************************************/
    double S0_yi0 = stepper0.currentPosition();
    double S1_yi0 = stepper1.currentPosition();
    double S0_yi = S.S0[S.frame_pointer];
    double S1_yi = S.S1[S.frame_pointer];
    double S0_yi1 = S.S0[S.frame_pointer+1];
    double S1_yi1 = S.S1[S.frame_pointer+1];
    double S0_vi0 = stepper0.speed();
    double S1_vi0 = stepper1.speed();
    // double S0_vi0 = 0;
    // double S1_vi0 = 0;
    double S0_vi = 0;
    double S1_vi = 0;
    // if (S.frame_pointer > 1){
    //   double S0_dy0 = S0_yi0-S.S0[S.frame_pointer-2];
    //   double S1_dy0 = S1_yi0-S.S1[S.frame_pointer-2];
    //   double S0_dy1 = S0_yi-S0_yi0;
    //   double S1_dy1 = S1_yi-S1_yi0;
    //   // double S0_dy0 = S0_yi-S.S0[S.frame_pointer-1];
    //   // double S1_dy0 = S1_yi-S.S1[S.frame_pointer-1];
    //   double epsilon = 0.00001;
    //   if (S0_dy1*S0_dy1 > epsilon && S0_dy0*S0_dy0 > epsilon && sign(S0_dy0)==sign(S0_dy1))
    //     S0_vi0 = 2/(1/(S.fps*S0_dy1)+1/(S.fps*S0_dy0)); // otherwise vi=0 (division by infinity)
    //     // S0_vi = (S0_yi1-S0_yi0)*2*S.fps;
    //   if (S1_dy1*S1_dy1 > epsilon && S1_dy0*S1_dy0 > epsilon) 
    //     S1_vi0 = 2/(1/(S.fps*S1_dy1)+1/(S.fps*S1_dy0)); // otherwise vi=0
    // }
    if (S.frame_pointer < S.n_frames-1){ // for the last frame, vi=0
      double S0_dy0 = S0_yi-S0_yi0;
      double S1_dy0 = S1_yi-S1_yi0;
      double S0_dy1 = S0_yi1-S0_yi;
      double S1_dy1 = S1_yi1-S1_yi;
      // double S0_dy0 = S0_yi-S.S0[S.frame_pointer-1];
      // double S1_dy0 = S1_yi-S.S1[S.frame_pointer-1];
      double epsilon = 0.00001;
      if (S0_dy1*S0_dy1 > epsilon && S0_dy0*S0_dy0 > epsilon && sign(S0_dy0)==sign(S0_dy1))
        S0_vi = 2/(1/(S.fps*S0_dy1)+1/(S.fps*S0_dy0)); // otherwise vi=0 (division by infinity)
        // S0_vi = (S0_yi1-S0_yi0)*2*S.fps;
      if (S1_dy1*S1_dy1 > epsilon && S1_dy0*S1_dy0 > epsilon) 
        S1_vi = 2/(1/(S.fps*S1_dy1)+1/(S.fps*S1_dy0)); // otherwise vi=0
        // S0_vi = (S0_yi1-S0_yi0)*2*S.fps;
    }
    double S0_ai = 2*S.fps*(2*S0_vi+S0_vi0) - 6*(S0_yi-S0_yi0)*S.fps*S.fps;
    double S1_ai = 2*S.fps*(2*S1_vi+S1_vi0) - 6*(S1_yi-S1_yi0)*S.fps*S.fps;
    double S0_ai0 = -2*S.fps*(S0_vi+2*S0_vi0) + 6*(S0_yi-S0_yi0)*S.fps*S.fps;
    double S1_ai0 = -2*S.fps*(S1_vi+2*S1_vi0) + 6*(S1_yi-S1_yi0)*S.fps*S.fps;

    double S0_aa = (S0_ai-S0_ai0)*S.fps;
    double S1_aa = (S1_ai-S1_ai0)*S.fps;
    // char str[100];
    // sprintf(str,"INFO: Frame: %d\tvalue: ")
    // if(S.frame_pointer==S.n_frames-1){
    // }
    double ti = 1/(double)S.fps; // time in seconds
    double ti0 = 0;
    S0.t = ti0;
    S1.t = ti0;
    S0.d = S0_aa/6;
    S1.d = S1_aa/6;
    S0.c = (ti*S0_ai0 - ti0*S0_ai)*S.fps/2;
    S1.c = S1_ai0;
    S0.b = ((S0_yi-S0_yi0) - S0.c*(ti*ti - ti0*ti0) - S0.d*(ti*ti*ti - ti0*ti0*ti0))*S.fps;
    S1.b = S1_vi0;
    S0.a = S0_yi0 - S0.b*ti0 - S0.c*ti0*ti0 - S0.d*ti0*ti0*ti0;
    S1.a = S1_yi0;
    // stepper0.setAcceleration(S0_RUN_ACC);
    // stepper1.setAcceleration(S1_RUN_ACC);
    // stepper0.setSpeed()
    stepper0.setAcceleration(2*S0.c);
    stepper1.setAcceleration(2*S1.c);
    stepper0.setAccelerationDeriv(6*S0.d);
    stepper1.setAccelerationDeriv(6*S1.d);
    stepper0.runMillis(1000/S.fps);
    stepper1.runMillis(1000/S.fps);
    stepper0.moveTo(S.S0[S.frame_pointer]);//in case of interrupt
    stepper1.moveTo(S.S1[S.frame_pointer]);
    stepper0.setSpeed(S0_vi0);
    stepper1.setSpeed(S1_vi0);
    // stepper0.setMaxSpeed(S0_MAX_SPD);
    // double S0_delta_x = S.S0[S.frame_pointer]-stepper0.currentPosition();
    // double S1_delta_x = S.S1[S.frame_pointer]-stepper1.currentPosition();
    // // double S0_cspd = stepper0.speed(); //current speed
    // // double S1_cspd = stepper1.speed();
    // double S0_next_delta_x = 0;
    // double S1_next_delta_x = 0;
    // if (S.frame_pointer<S.n_frames-1){
    //   S0_next_delta_x = S.S0[S.frame_pointer+1]-S.S0[S.frame_pointer];
    //   S1_next_delta_x = S.S1[S.frame_pointer+1]-S.S1[S.frame_pointer];
    //   stepper0.moveTo(S.S0[S.frame_pointer]);//in case of interrupt
    //   stepper1.moveTo(S.S1[S.frame_pointer]);
    // }
    // double S0_cspd = (S0_delta_x)*S.fps; //current spd
    // double S1_cspd = (S1_delta_x)*S.fps;
    // double S0_nspd = (S0_next_delta_x)*S.fps; //speed for next interval
    // double S1_nspd = (S1_next_delta_x)*S.fps;
    // double S0_acc = abs((S0_nspd-S0_cspd)*S.fps);
    // double S1_acc = abs((S1_nspd-S1_cspd)*S.fps);
    // double S0_acc = 2*(S0_delta_x-stepper0.speed()/S.fps)*S.fps*S.fps;
    // double S1_acc = 2*(S1_delta_x-stepper1.speed()/S.fps)*S.fps*S.fps;
    // if(S.frame_pointer==S.n_frames-2){
    //   S0_acc = -stepper0.speed()*S.fps;
    //   S1_acc = -stepper1.speed()*S.fps;
    // }
    // stepper0.setAcceleration(0);
    // stepper1.setAcceleration(0);
    // stepper0.setSpeed(abs(S0_cspd));
    // stepper0.setMaxSpeed(abs(S0_cspd));
    // stepper1.setMaxSpeed(S1_cspd);
    

    digitalWrite(D_0,S.D0[S.frame_pointer]);
    digitalWrite(D_1,S.D1[S.frame_pointer]);
    digitalWrite(D_2,S.D2[S.frame_pointer]);
    digitalWrite(D_3,S.D3[S.frame_pointer]);
    
    return;
    // TODO Check ESTOP signal
    // stepper1.run(); 
   
}

void interpolationMove(){
  /****************************************************
   x(t)= x0 + v*t + 1/2*a*t^2 + 1/6*a'*t^3
  ****************************************************/
  // int S0_pos = (int)(S0.a + S0.b*S0.t + S0.c*(S0.t*S0.t) + S0.d*(S0.t*S0.t*S0.t));
  // // int S1_pos = (int)(S1.a + S1.b*S1.t + S1.c*(S1.t*S1.t) + S1.d*(S1.t*S1.t*S1.t));
  // // double V0_pos = S0.b + S0.c*S0.t + S0.d*(S0.t*S0.t);
  // // stepper0.setMaxSpeed(V0_pos);
  // // stepper0.moveTo(S0_pos);
  // // stepper1.moveTo(S1_pos);
  // char s[100];
  // int x = stepper0.currentPosition();
  // double v = stepper0.speed();
  // // double a = stepper0.acceleration();
  // // double aa = stepper0.accelerationDeriv();
  // sprintf(s,"%lf\t%d\t%d\t%lf\n",S0.t+(double)(S.frame_pointer-1)/S.fps,x,S0_pos,v);
  // Serial.print(s);
  // S0.t+=0.001;
  // S1.t+=0.001;
}

/*
void displayProgram() {
  int numFrames = EEPROM.read(BNUM_FRAMES_ADDR);
  int fps = EEPROM.read(BLENDER_FPS_ADDR);
  int frameDelay=1000/fps; // 1000 ms/fps
  
  SerialCtrl.print("FRAMES: ");
  SerialCtrl.println(numFrames);
  SerialCtrl.print("FPS: ");
  SerialCtrl.println(fps, DEC);
  SerialCtrl.print("DELAY: ");
  SerialCtrl.println(frameDelay, DEC);
  
  SerialCtrl.println("BEGIN");
  
  for(int i=0; i<numFrames; i++) {
    int val = EEPROM.read(FRAME_START_ADDR + i);
    SerialCtrl.print("FOCUS POINT: ");
    SerialCtrl.println(val, DEC);
  }
  
  SerialCtrl.println("END");
}

*/