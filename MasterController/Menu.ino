
void display_refresh(){
 lcd.setCursor(0,0);  
 lcd.print("                ");
 menurefreshed = true;
 lcd.setCursor(0,1);
 lcd.print("                ");
}



void menuChanged(MenuChangeEvent changed){
 display_refresh();
 MenuItem newMenuItem=changed.to; //get the destination menu
 
 lcd.setCursor(0,0); 
 
 if(newMenuItem.getName()==menu.getRoot()){
     lcd_default();
 }else if(newMenuItem.getName()=="Analog sens"){
     lcd.print("  Analog sens   ");
 }else if(newMenuItem.getName()==">"){
     lcd.print("       ->       ");
 }else if(newMenuItem.getName()=="<"){
     lcd.print("       <-       ");
 }else if(newMenuItem.getName()=="Digital sens"){
     lcd.print("  Digital Sens  ");
 }else if(newMenuItem.getName()==">"){
     lcd.print("       ->       ");
 }else if(newMenuItem.getName()=="<"){
     lcd.print("       <-       ");
 }else if(newMenuItem.getName()=="Sound"){
     lcd.print("     Sound      ");
 }else if(newMenuItem.getName()=="on"){
     lcd.print("       on       ");
     // EEPROM.write(3,1);
 }else if(newMenuItem.getName()=="off"){
     lcd.print("       off      ");
     // EEPROM.write(3,0);
 }
  
}

void menuUsed(MenuUseEvent used){
 lcd.setCursor(0,0);  
 lcd.print("You used        ");
 lcd.setCursor(0,1); 
 lcd.print(used.item.getName());
 delay(3000);  
 lcd.setCursor(0,0);  
 lcd.print("Settings        ");
 menu.toRoot();  
 lcd_default();
}


void  readButtons(){  //read buttons status
 int reading;
 int buttonEnterState=HIGH;             
 int buttonEscState=HIGH;            
 int buttonLeftState=HIGH;           
 int buttonRightState=HIGH;          
 
 reading = digitalRead(KB_PIN);
 if (reading != lastButtonEnterState) {
   lastEnterDebounceTime = millis();
 } 
 if ((millis() - lastEnterDebounceTime) > debounceDelay) {
   buttonEnterState=reading;
   lastEnterDebounceTime=millis();
 }
 lastButtonEnterState = reading;
 reading = digitalRead(ESC_PIN);
 
if (reading != lastButtonEscState) {
   lastEscDebounceTime = millis();
 } 
 
 if ((millis() - lastEscDebounceTime) > debounceDelay) {
   buttonEscState = reading;
   lastEscDebounceTime=millis();
 }
 lastButtonEscState = reading; 
 reading = digitalRead(DOWN_PIN);
 if (reading != lastButtonRightState) {
   lastRightDebounceTime = millis();
 } 
 
 if ((millis() - lastRightDebounceTime) > debounceDelay) {
   buttonRightState = reading;
  lastRightDebounceTime =millis();
 }
 lastButtonRightState = reading;                  
 reading = digitalRead(UP_PIN);
 if (reading != lastButtonLeftState) {
   lastLeftDebounceTime = millis();
 } 
 
 if ((millis() - lastLeftDebounceTime) > debounceDelay) {
   buttonLeftState = reading;
   lastLeftDebounceTime=millis();;
 }
 lastButtonLeftState = reading;  

 if (buttonEnterState==LOW){
   lastButtonPushed=KB_PIN;

 }else if(buttonEscState==LOW){
   lastButtonPushed=ESC_PIN;

 }else if(buttonRightState==LOW){
   lastButtonPushed=DOWN_PIN;

 }else if(buttonLeftState==LOW){
   lastButtonPushed=UP_PIN;

 }else{
   lastButtonPushed=0;
 }                  
 
}

void navigateMenus() {
 MenuItem currentMenu=menu.getCurrent();
 switch (lastButtonPushed){
   case KB_PIN:
     if(!(currentMenu.moveDown())){  //if the current menu has a child and has been pressed enter then menu navigate to item below
       menu.use();
     }else{  //otherwise, if menu has no child and has been pressed enter the current menu is used
       menu.moveDown();
      } 
     break;
   case ESC_PIN:
     if(!(currentMenu.moveUp())){
       menu.toRoot();  
     }else{
       menu.moveUp();
     }
     break;
   case DOWN_PIN:
     menu.moveRight();
     break;      
   case UP_PIN:
     menu.moveLeft();
     break;      
 }
 
 lastButtonPushed=0; 
}

void lcd_default(){
 lcd.setCursor(0,0);  
 lcd.print("SloMoments Robo1");
  menurefreshed = true;
 lcd.setCursor(0,1);
 lcd.print("                ");
}
