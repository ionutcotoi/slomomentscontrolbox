void PCSerialCom(){
  int frame,value; // temporary store the frame and value from a command that's passed to the device
  memset(cmd, 0, sizeof(cmd));
    //SerialCtrl.print("STRING: ");
    //SerialCtrl.println(inputString); 
    inputString.toCharArray(charBuf, 50) ;
      
    strncpy(cmd, charBuf, CMD_LEN);
      
    ioType = charBuf[CMD_LEN+1];
    ioNum  = atoi(charBuf+CMD_LEN+2);

    SerialPC.print("CMD: ");
    SerialPC.println(cmd);

    SerialPC.print("TYPE: ");
    SerialPC.println(ioType);

    SerialPC.print("NUM: ");
    SerialPC.println(ioNum, DEC);

    //SerialCtrl.print("CMD: ");
    //SerialCtrl.println(cmd);
    
    if(strcmp(cmd, "MOVE") == 0) { // ****************************** MOVE **********************
        // debugging purpuses: change outputs accordingly
        
        value = atoi(charBuf+CMD_LEN+4);
        switch(ioType){
            case 'F':
              if (value < 0 || value > MAX_FOCUS_VALUE){
                //error: report bad value
                SerialPC.print("ERROR: MOVE command, bad value\n\tvalue should be between 0 and 255, given ");
                SerialPC.println(value);
              } else {
                char tmp[100];
                if (ioNum == 0 || ioNum ==1){
                  sprintf(tmp,"F%d %d\n", ioNum, value);
                  Serial2.print(tmp);
                } else {
                  // error: report bad command
                  SerialPC.println("ERROR: output value should be one of: F0, F1, S0, S1, D0, D1, D2, D3");
                }
              }
              break;
            case 'S':
              if (value < MIN_STEPPER_VAL || value > MAX_STEPPER_VAL){
                //error: report bad value
                char tmps[100];
                sprintf(tmps, "ERROR: MOVE command, bad value\n\tvalue should be between %d and %d, given %d",
                            MIN_STEPPER_VAL,MAX_STEPPER_VAL, value);
                SerialPC.println(tmps);
              } else {
                if (ioNum == 0){
                  stepper0.setMaxSpeed(S0_MAX_SPD/10);
                  stepper0.setAcceleration(S0_MAX_ACC/5);
                  stepper0.moveTo(value);
                } else if (ioNum == 1){
                  stepper1.setMaxSpeed(S1_MAX_SPD/10);
                  stepper1.setAcceleration(S1_MAX_ACC/5);
                  stepper1.moveTo(value);
                } else {
                  // error: report bad command
                  SerialPC.println("ERROR: output value should be one of: F0, F1, S0, S1, D0, D1, D2, D3");
                }
              }
              break;
            case 'D':
              if (value < 0 || value > 1){
                  //error: report bad value
                  SerialPC.println("ERROR: MOVE command, bad value for digital outputs: value should be 0 or 1");
              } else {
                if (ioNum == 0){
                   digitalWrite(D_0,value);
                } else if (ioNum == 1){
                   digitalWrite(D_1,value);
                } else if (ioNum == 2){
                   digitalWrite(D_2,value);
                } else if (ioNum == 3){
                   digitalWrite(D_3,value);
                } else {
                  // error: report bad command
                  SerialPC.println("ERROR: output value should be one of: F0, F1, S0, S1, D0, D1, D2, D3");
                }
              }
              break;
            default:
              // error: report bad command
              SerialPC.println("ERROR: output value should be one of: F0, F1, S0, S1, D0, D1, D2, D3");
          }
          
    } else if (strcmp(cmd, "STOR") == 0){ // ************************** STOR **********************
        // store in RAM
        frame = atoi(charBuf+CMD_LEN+4);
        char *pos = charBuf + CMD_LEN +4;
        while(*pos!=' ') pos++;
        value = atoi(pos);
        if (frame < 0 || frame >= MAX_FRAMES){
          SerialPC.println("ERROR: frame out of bounds");
          SerialPC.print("\tframe: ");
          SerialPC.println(frame);
        } else {
          switch(ioType){
            case 'F':
              if (value < 0 || value > MAX_FOCUS_VALUE){
                //error: report bad value
                SerialPC.println("ERROR: value for focus outputs should be between 0 and 255");
                SerialPC.print("\tvalue: ");
                SerialPC.println(value);
              } else {
                if (ioNum == 0){
                  S.F0[frame] = value;
                } else if (ioNum == 1){
                  S.F1[frame] = value;
                } else {
                  SerialPC.println("ERROR: output value should be one of: F0, F1, S0, S1, D0, D1, D2, D3");
                }
              }
              break;
            case 'S':
              if (value < MIN_STEPPER_VAL || value > MAX_STEPPER_VAL){
                //error: report bad value
                SerialPC.println("ERROR: value for steppers out of bounds");
                char tmps[100];
                sprintf(tmps,"\tvalue should be between %d and %d, given %d",
                MIN_STEPPER_VAL,MAX_STEPPER_VAL,value);
                SerialPC.println(tmps);
              } else {
                if (ioNum == 0){
                  S.S0[frame] = value;
                } else if (ioNum == 1){
                  S.S1[frame] = value;
                } else {
                  // error: report bad command
                  SerialPC.println("ERROR: output value should be one of: F0, F1, S0, S1, D0, D1, D2, D3");
                }
              }
              break;
            case 'D':
              if (value < 0 || value > 1){
                  //error: report bad value
              } else {
                if (ioNum == 0){
                  S.D0[frame] = value;
                } else if (ioNum == 1){
                  S.D1[frame] = value;
                } else if (ioNum == 2){
                  S.D2[frame] = value;
                } else if (ioNum == 3){
                  S.D3[frame] = value;
                } else {
                  // error: report bad command
                }
              }
              break;
            default:
              // error: report bad command
              SerialPC.println("ERROR: output value should be one of: F0, F1, S0, S1, D0, D1, D2, D3");
          }
        }    
    } else if (strcmp(cmd, "BFPS") == 0){ //*************************  BFPS  *********************
        // store fps and no_of_frames
        int fps = atoi(charBuf + CMD_LEN + 1);
        char *pos = charBuf + CMD_LEN +1;
        while(*pos!=' ') pos++; 
        int n_frames = atoi(pos);
        if (fps > MAX_FPS || fps < 1 || n_frames < 1 || n_frames > MAX_FRAMES){
          SerialPC.println("ERROR: BFPS <fps> <no_of_frames>");
          SerialPC.print("\tfps should be between 1 and ");
          SerialPC.println(MAX_FPS);
          SerialPC.print("\tno_of_frames should be between 1 and ");
          SerialPC.println(MAX_FRAMES);
        } else {
          S.fps = fps;
          S.n_frames = n_frames;
        }
    } else if (strcmp(cmd, "STOP") == 0){//*****************************  STOP ************
        // stop the program
        SerialCtrl.println("STOP: program stopped!");
        runProgThread.enabled = false;
        stepperInterpolationThread.enabled = false;
        S.frame_pointer = 0;
    } else if (strcmp(cmd, "SHOW") == 0){ // ****************************  SHOW  *******************
        // display configuration on the serial port
        cmd_SHOW();
    } else if (strcmp(cmd, "CLER") == 0){ // **************************  CLER  ****************
        // CLEAR, erases stored configuration
        cmd_CLER();
    } else if (strcmp(cmd, "READ") == 0){// ----------------------- TODO: READ  ------------------
      // read inputs
    } else if (strcmp(cmd, "RRUN") == 0){// **************************  RRUN  *********************
      SerialCtrl.println("RRUN: running...");
      cmd_RRUN();
    } else if (strcmp(cmd, "INIT") == 0){ //**************************  INIT  *********************
      cmd_INIT();
    } else if (strcmp(cmd, "PARM") == 0){ //**************************  PARM  *********************
      char param[10];
      strncpy(param,charBuf + CMD_LEN + 1,CMD_LEN);
      param[CMD_LEN]='\0';
      value = atoi(charBuf + 2*CMD_LEN+2);

      cmd_PARM(param, value);
    } else{
        SerialPC.println("ERROR: command unknown");
        SerialPC.println("the following commands are available:");
        SerialPC.println("MOVE <output> <value> : manually set output to value");
        SerialPC.println("STOR <output> <frame> <value> : store a value that will be sent to output at the corresponding frame");
        SerialPC.println("BFPS <fps> <number_of_frames> : set frames per second and number of frames ");
         
    }
    inputString = "";
}


// void Serial3Event() {
void Serial3Event() {
  while (SerialPC.available()) {
    // get the new byte:
    char inChar = (char)SerialPC.read(); 
    //Serial.print(inChar);
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if ((inChar == '\n') || (inChar == '\r') || (inChar == '\\')) {
      stringComplete = true;
    } 
  }
}
