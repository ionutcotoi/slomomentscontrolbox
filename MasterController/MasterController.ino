#include <avr/eeprom.h>
#include <Wire.h>
#include <LiquidCrystal.h>
#include <MenuBackend.h>
#include <SerialEvent.h>
#include <SPI.h>
#include <Thread.h>
#include <AccelStepper.h>


#define ESC_PIN    13
#define KB_PIN     14
#define DOWN_PIN   15
#define UP_PIN     16
#define pinEstop   6
#define pinBuzzer 12
#define sdaLcd    18
#define sclLcd    19

#define soundActive tone(pinBuzzer, 2000, 200)
#define noSound noTone(pinBuzzer)

#define SerialCtrl   Serial
#define SerialPC     Serial3
#define SerialRobot  Serial1
#define SerialFocus  Serial2

#define CMD_LEN 4
uint32_t* S0_SPD_ADDR = (uint32_t*)0;
uint32_t* S0_ACC_ADDR = (uint32_t*)4;
uint32_t* S1_SPD_ADDR = (uint32_t*)8;
uint32_t* S1_ACC_ADDR = (uint32_t*)12;
uint32_t* RRUN_DELAY_ADDR = (uint32_t*)16;
//next eeprom free addr: 20

#define MAX_NUM_IO_PINS  8

#define OUTPUT_STEP_0      23
#define OUTPUT_STEP_DIR_0  21
#define OUTPUT_STEP_1      22
#define OUTPUT_STEP_DIR_1  20

#define MAX_FRAMES 1000 // max no of frames that can be stored in the device
#define MAX_FPS 25      // max fps at which the device opperates

#define MIN_STEPPER_VAL -1000000
#define MAX_STEPPER_VAL 1000000

#define MAX_FOCUS_VALUE 10000

#define D_0 2
#define D_1 3
#define D_2 4
#define D_3 5

#define S0_RUN_ACC 1000000
#define S1_RUN_ACC 1000000

int S0_MAX_SPD = eeprom_read_dword(S0_SPD_ADDR);
int S0_MAX_ACC = eeprom_read_dword(S0_ACC_ADDR);
int S1_MAX_SPD = eeprom_read_dword(S1_SPD_ADDR);
int S1_MAX_ACC = eeprom_read_dword(S1_ACC_ADDR);
int RRUN_DELAY = eeprom_read_dword(RRUN_DELAY_ADDR);

boolean estop = false;
LiquidCrystal lcd(0);

String inputString = "";         // a string to hold incoming data
String inputRobot = "";
boolean stringComplete = false;  // whether the string is complete
boolean stringRobotComplete = false;

unsigned analogSensorsTotal = 16;
unsigned analogIn[] = {A0};
unsigned analogVal[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned digitalIn[] = {22, 24, 26, 28, 30, 32, 34, 36};
unsigned digitalOut[] = {23, 25, 27, 29, 31, 33, 35, 37};
unsigned digitalVal[] = {0, 0, 0, 0, 0, 0, 0, 0};
// unsigned keypadIn[] = {2, 3, 4, 5};

int sensorPin1 = A0; 
int sensorPin2 = A1; 
int sensorPin3 = A2; 
int sensorPin4 = A3; 
int sensorPin5 = A4; 
int sensorPin6 = A5; 
int sensorPin7 = A6; 
int sensorPin8 = A7;

int sensor1 = 0; 
int sensor2 = 0; 
int sensor3 = 0; 
int sensor4 = 0; 
int sensor5 = 0; 
int sensor6 = 0; 
int sensor7 = 0; 
int sensor8 = 0;

int lastButtonPushed = 0;

int lastButtonEnterState = LOW;  
int lastButtonEscState = LOW;   
int lastButtonLeftState = LOW;   
int lastButtonRightState = LOW;   

long lastEnterDebounceTime = 0;  
long lastEscDebounceTime = 0;  
long lastLeftDebounceTime = 0;  
long lastRightDebounceTime = 0;  
long debounceDelay = 100;

bool menurefreshed = true;

MenuBackend menu = MenuBackend(menuUsed,menuChanged);

      MenuItem miAnalog = MenuItem("Analog sens");
            MenuItem miASens1 = MenuItem(">");
            MenuItem miASens2 = MenuItem("<");

      MenuItem miDigital = MenuItem("Digital sens");
            MenuItem miDSens1 = MenuItem(">");
            MenuItem miDSens2 = MenuItem("<");

      MenuItem miSound = MenuItem("Sound");
            MenuItem miOn = MenuItem("on");
            MenuItem miOff = MenuItem("off");



int totalF1 = 0;
int readingsF1[10];
int indexF1 = 0;
int averageF1 = 0;
int valoriF1[100];
int indexvalF1 = 0;
int val_ant1;

AccelStepper stepper0(2, OUTPUT_STEP_0, OUTPUT_STEP_DIR_0);
AccelStepper stepper1(2, OUTPUT_STEP_1, OUTPUT_STEP_DIR_1);

int value = 0;
int numFrames = 0;
int frame = 0;

char cmd[CMD_LEN+1] = {0};
char cmdRobot[CMD_LEN+1] = {0};

char ioType = ' ';
char ioNum = 0;

char numIOUsed = 0;

char charBuf[50];
char charBufRobot[50];

struct stored_program {
  int        F0[MAX_FRAMES];
  int        F1[MAX_FRAMES];
  int        S0[MAX_FRAMES];
  int        S1[MAX_FRAMES];
  byte       D0[MAX_FRAMES];
  byte       D1[MAX_FRAMES];
  byte       D2[MAX_FRAMES];
  byte       D3[MAX_FRAMES];
  int        fps = 0; //fps rate
  int        n_frames = 0; //total no of frames
  int        frame_pointer = 0;
};

struct motion_eq_coef {
  double a;
  double b;
  double c;
  double d;
  double t;
};

Thread runProgThread = Thread();
Thread stepperInterpolationThread = Thread();

stored_program S;

motion_eq_coef S0,S1;

void setup(){
  initialise_pins();
  initialise_features();
  //initialiseAnalogTune();
  //for (int i = 0; i < 10; i++)
  //  readingsF1[i] = 0; 
  //Serial.begin(115200);
  SerialCtrl.begin(115200);
  SerialPC.begin(115200);
  SerialRobot.begin(115200);
  Serial2.begin(115200);
  pinMode(D_0, OUTPUT);
  pinMode(D_1, OUTPUT);
  pinMode(D_2, OUTPUT);
  pinMode(D_3, OUTPUT);
  pinMode(OUTPUT_STEP_0, OUTPUT);
  pinMode(OUTPUT_STEP_DIR_0, OUTPUT);
  pinMode(OUTPUT_STEP_1, OUTPUT);
  pinMode(OUTPUT_STEP_DIR_1, OUTPUT);
  // reserve 200 bytes for the inputString:
  inputString.reserve(30);
  delay(1000);
  SerialPC.println("OK");
  SerialPC.println("ROBOT INTF OK");
  //SerialFocus.print("t");
  lcd.begin(16,2);
  lcd.setBacklight(HIGH);
  lcd_default();
  pinMode(UP_PIN, INPUT_PULLUP);
  pinMode(DOWN_PIN, INPUT_PULLUP);
  pinMode(KB_PIN, INPUT_PULLUP);
  pinMode(ESC_PIN, INPUT_PULLUP);
  stepper0.setMaxSpeed(S0_MAX_SPD);
  stepper0.setAcceleration(S0_MAX_ACC);
  stepper0.setCurrentPosition(0);
  stepper1.setMaxSpeed(S1_MAX_SPD);
  stepper1.setAcceleration(S1_MAX_ACC);
  stepper1.setCurrentPosition(0);

  
  SerialPC.println("Done init motor");
  runProgThread.onRun(runProgram);
  runProgThread.enabled=false;
  stepperInterpolationThread.onRun(interpolationMove);
  stepperInterpolationThread.enabled=false;
  SerialCtrl.println("Control Serial enabled");
}


void loop() {
  if(emergencyStop()) {
    alarm();
    } else {
      stepper0.run();
      stepper1.run();
    if(stringRobotComplete) {
      RobotSerialCom();
      stringRobotComplete = false;
    }
    if(stringComplete) {
      PCSerialCom();
      stringComplete = false;
    }
    readButtons(); 
    navigateMenus(); 
    lcd.setCursor(0,1);
    lcd.print(lastButtonPushed);   
    //noTone(pinBuzzer);
    //processAnalogInputs();
     // if (!menurefreshed){
     //  lcd_default();
     // }
    if (runProgThread.shouldRun()){
      runProgThread.run();
    }
    if (stepperInterpolationThread.shouldRun()){
      stepperInterpolationThread.run();
    }
  }
  Serial1Event();
  Serial3Event();
}
