void RobotSerialCom(){
  SerialCtrl.println(inputRobot);
    memset(cmdRobot, 0, sizeof(cmd));
    inputRobot.toCharArray(charBufRobot, 50) ;
    strncpy(cmdRobot, charBufRobot, CMD_LEN);
    if(strcmp(cmdRobot, "RRUN") == 0) {
      SerialCtrl.println("Robot: RRUN");
      delay(RRUN_DELAY);
      cmd_RRUN();
    } else if (strcmp(cmdRobot, "INIT") == 0) {
      SerialCtrl.println("Robot: INIT");
      cmd_INIT();
    }
    inputRobot = "";
}

void Serial1Event() {
  while (SerialRobot.available()) {
    // get the new byte:
    char inChar = (char)SerialRobot.read(); 
    // add it to the inputString:
    inputRobot += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if ((inChar == '\n') || (inChar == '\r')) {
      stringRobotComplete = true;
    } 
  }  
}
