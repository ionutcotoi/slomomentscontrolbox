void cmd_INIT(){
	stepper0.setAcceleration(S0_MAX_ACC);
  stepper1.setAcceleration(S1_MAX_ACC);
	stepper0.setMaxSpeed(S0_MAX_SPD);
  stepper1.setMaxSpeed(S1_MAX_SPD);
  
  stepper0.moveTo(S.S0[0]);
  stepper1.moveTo(S.S1[0]);  
  
  //digitalPotWrite(F0_CSS,S.F0[0]);
  //digitalPotWrite(F1_CSS,S.F1[0]);
  SerialFocus.print("F0 ");
  SerialFocus.println(S.F0[0]);
  SerialFocus.print("F1 ");
  SerialFocus.println(S.F1[0]);
  S.frame_pointer = 0;

}

void cmd_RRUN(){
  if (S.fps != 0){
    runProgThread.enabled = true;
    runProgThread.setInterval(1000/S.fps);
    stepperInterpolationThread.enabled=true;
    stepperInterpolationThread.setInterval(1);
  }
  else {
    SerialPC.println("ERROR: RRUN command: fps not set. Send BFPS <fps> <number_of_frames>");
  }
}

void cmd_PARM(char* param, int value){
	eeprom_busy_wait();
	SerialPC.print("PARM command: param = ");
	SerialPC.println(param);
	if(strcmp(param,"S0SP")==0){
		SerialPC.println("S0 SPD....");
	    eeprom_update_dword(S0_SPD_ADDR,value);
      S0_MAX_SPD = value;
	} else if (strcmp(param,"S0AC")==0){
    SerialPC.println("S0 ACC....");
	    eeprom_update_dword(S0_ACC_ADDR,value);
      S0_MAX_ACC = value;
	} else if (strcmp(param,"S1SP")==0){
    SerialPC.println("S1 SPD....");
	    eeprom_update_dword(S1_SPD_ADDR,value);
      S1_MAX_SPD = value;
	} else if (strcmp(param,"S1AC")==0){
    SerialPC.println("S1 ACC....");
	    eeprom_update_dword(S1_ACC_ADDR,value);
      S1_MAX_ACC = value;
	} else if (strcmp(param,"DLAY")==0){
    SerialPC.println("Delay value stored to eeprom.");
      eeprom_update_dword(RRUN_DELAY_ADDR,value);
      RRUN_DELAY = value;
  }
}

void cmd_SHOW(){
	SerialPC.println("SHOW command");
    char tmps[100];
    SerialPC.println("F0\tF1\tS0\tS1\tD0\tD1\tD2\tD3");
    for (int frm=0; frm<S.n_frames; frm++){
      sprintf(tmps,"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d", \
      S.F0[frm], S.F1[frm], S.S0[frm], S.S1[frm], \
      S.D0[frm], S.D1[frm],S.D2[frm],S.D3[frm]);
      SerialPC.println(tmps);
    }
    sprintf(tmps,"fps = %d\nframes = %d",S.fps,S.n_frames);
    SerialPC.println(tmps);
    int s0sp = (int)eeprom_read_dword(S0_SPD_ADDR);
    int s0ac = (int)eeprom_read_dword(S0_ACC_ADDR);
    int s1sp = (int)eeprom_read_dword(S1_SPD_ADDR);
    int s1ac = (int)eeprom_read_dword(S1_ACC_ADDR);
    int dly = (int)eeprom_read_dword(RRUN_DELAY_ADDR);
    sprintf(tmps,"S0 spd: %d\nS0 acc: %d\nS1 spd: %d\nS1 acc: %d\nRRUN delay: %d\n", s0sp,s0ac,s1sp,s1ac,dly);
    SerialPC.println(tmps);
}

void cmd_CLER(){
	for (int frm=0; frm<MAX_FRAMES; frm++){
      S.F0[frm]=0;
      S.F1[frm]=0;
      S.S0[frm]=0;
      S.S1[frm]=0;
      S.D0[frm]=0; 
      S.D1[frm]=0;
      S.D2[frm]=0;
      S.D3[frm]=0;
    }
    S.fps = 0;
    S.n_frames = 0;
    SerialPC.println("program cleared");
}