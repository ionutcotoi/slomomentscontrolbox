void initialiseAnalogDetection(){
  pinMode(A0, INPUT_PULLUP);
  pinMode(A1, INPUT_PULLUP);
  pinMode(A2, INPUT_PULLUP);
  pinMode(A3, INPUT_PULLUP);
  pinMode(A4, INPUT_PULLUP);
  pinMode(A5, INPUT_PULLUP);
  pinMode(A6, INPUT_PULLUP);
  pinMode(A7, INPUT_PULLUP);
}

char AnalogDetection(){
  sensor1 = analogRead(sensorPin1);
  sensor2 = analogRead(sensorPin2);
  sensor3 = analogRead(sensorPin3);
  sensor4 = analogRead(sensorPin4);
  sensor5 = analogRead(sensorPin5);
  sensor6 = analogRead(sensorPin6);
  sensor7 = analogRead(sensorPin7);
  sensor8 = analogRead(sensorPin8);
 
  if(sensor1 > 1000)
    Serial.print("not s1 - ");
  else
    Serial.print("analog1 - ");
  
  if(sensor2 > 1000) 
    Serial.print("not s2 - "); 
  else
    Serial.print("analog2 - ");
  
  if(sensor3 > 1000) 
    Serial.print("not s3 - "); 
  else
    Serial.print("analog3 - ");
  
  if(sensor4 > 1000) 
    Serial.print("not s4 - "); 
  else
    Serial.print("analog4 - ");
    
  if(sensor5 > 1000) 
    Serial.print("not s5 - "); 
  else
    Serial.print("analog5 - ");
        
  if(sensor6 > 1000) 
    Serial.print("not s6 - "); 
  else
    Serial.print("analog6 - ");
    
  if(sensor7 > 1000) 
    Serial.print("not s7 - "); 
  else
    Serial.print("analog7 - ");
  
  if(sensor8 > 1000) 
    Serial.println("not s8"); 
  else
    Serial.println("analog8"); 
}


