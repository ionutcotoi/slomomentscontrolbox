#include <MenuBackend.h>
#include <Wire.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>

#define ESC_PIN    2
#define KB_PIN     3
#define DOWN_PIN   4
#define UP_PIN     5
#define pinEstop   7
#define pinBuzzer 12
#define txSerial  18
#define rxSerial  19
#define sdaLcd    20
#define sclLcd    21

#define soundActive tone(pinBuzzer, 2000, 200)
#define noSound noTone(pinBuzzer)

#define EEPROM_F1  1
#define EEPROM_F2  2
#define EEPROM_F3  3
#define EEPROM_F4  4
#define EEPROM_F5  5
#define EEPROM_F6  6
#define EEPROM_F7  7
#define EEPROM_F8  8

int sensorPin1 = A0; 
int sensorPin2 = A1; 
int sensorPin3 = A2; 
int sensorPin4 = A3; 
int sensorPin5 = A4; 
int sensorPin6 = A5; 
int sensorPin7 = A6; 
int sensorPin8 = A7;

int sensor1 = 0; 
int sensor2 = 0; 
int sensor3 = 0; 
int sensor4 = 0; 
int sensor5 = 0; 
int sensor6 = 0; 
int sensor7 = 0; 
int sensor8 = 0;

LiquidCrystal lcd(0);

unsigned analogSensorsTotal = 16;

unsigned digitalIn[]  = {39, 41, 43, 45, 47, 49, 51, 53};
unsigned digitalVal[] = {0, 0, 0, 0, 0, 0, 0, 0};

const int buttonPinLeft  = 5;     
const int buttonPinRight = 4;    
const int buttonPinEnter = 3;   
const int buttonPinEsc   = 2;     

int lastButtonPushed = 0;

int lastButtonEnterState = LOW;
int lastButtonEscState   = LOW;
int lastButtonLeftState  = LOW;
int lastButtonRightState = LOW;   

long lastEnterDebounceTime = 0;
long lastEscDebounceTime   = 0;
long lastLeftDebounceTime  = 0;
long lastRightDebounceTime = 0;  
long debounceDelay         = 100; 

unsigned long keyPrevMillis  = 0;
unsigned long keyPrevMillis1 = 0;

const unsigned long keySampleIntervalMs  = 20;
const unsigned long keySampleIntervalMs1 = 20;

byte longKeyPressCountMax   = 50;
byte longkeyPressCount1Max1 = 50; 

byte longKeyPressCount  = 0;
byte longkeyPressCount1 = 0;

byte prevKeyState  = HIGH;
byte prevKeyState1 = HIGH;         
const byte keyPin  = UP_PIN;                    
const byte keyPin1 = ESC_PIN;            

unsigned readings[] = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned index[] = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned total[] = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned average[] = {0, 0, 0, 0, 0, 0, 0, 0};


MenuBackend menu = MenuBackend(menuUsed,menuChanged);

      MenuItem miAnalog = MenuItem("Analog sens");
            MenuItem miASens1 = MenuItem(">");
            MenuItem miASens2 = MenuItem("<");
           
      MenuItem miDigital = MenuItem("Digital sens");
            MenuItem miDSens1 = MenuItem(">");
            MenuItem miDSens2 = MenuItem("<");

      MenuItem miSound = MenuItem("Sound");
            MenuItem miOn = MenuItem("on");
            MenuItem miOff = MenuItem("off");

void menuChanged(MenuChangeEvent changed){
  
  MenuItem newMenuItem=changed.to; //get the destination menu
  
  lcd.setCursor(0,1); 
  
  if(newMenuItem.getName()==menu.getRoot()){
      lcd.print("                ");
  }else if(newMenuItem.getName()=="Analog sens"){
      lcd.print("  Analog sens   ");
  }else if(newMenuItem.getName()==">"){
      lcd.print("       ->       ");
      EEPROM.write(1,1);
  }else if(newMenuItem.getName()=="<"){
      lcd.print("       <-       ");
      EEPROM.write(1,0);
  }else if(newMenuItem.getName()=="Digital sens"){
      lcd.print("  Digital Sens  ");
  }else if(newMenuItem.getName()==">"){
      lcd.print("       ->       ");
      EEPROM.write(2,1);
  }else if(newMenuItem.getName()=="<"){
      lcd.print("       <-       ");
      EEPROM.write(2,0);
  }else if(newMenuItem.getName()=="Sound"){
      lcd.print("     Sound      ");
  }else if(newMenuItem.getName()=="on"){
      lcd.print("       on       ");
      EEPROM.write(3,1);
  }else if(newMenuItem.getName()=="off"){
      lcd.print("       off      ");
      EEPROM.write(3,0);
  }
}

void menuUsed(MenuUseEvent used){
  lcd.setCursor(0,0);  
  lcd.print("You used        ");
  delay(2000);  
  initialise_features();  
}

void  readButtons(){  
  int reading;
  int buttonEnterState = HIGH;
  int buttonEscState   = HIGH;
  int buttonLeftState  = HIGH;
  int buttonRightState = HIGH;          
  
                  reading = digitalRead(buttonPinEnter);
                  
                  if (reading != lastButtonEnterState) {
                    lastEnterDebounceTime = millis();
                  } 
                  
                  if ((millis() - lastEnterDebounceTime) > debounceDelay) {
                    buttonEnterState=reading;
                    lastEnterDebounceTime=millis();
                  }
                  
                  lastButtonEnterState = reading;
                  reading = digitalRead(buttonPinEsc);
                  
                 if (reading != lastButtonEscState) {
                    lastEscDebounceTime = millis();
                  } 
                  
                  if ((millis() - lastEscDebounceTime) > debounceDelay) {
                    buttonEscState = reading;
                    lastEscDebounceTime=millis();
                  }
                  
                  lastButtonEscState = reading; 
                  reading = digitalRead(buttonPinRight);
                  
                  if (reading != lastButtonRightState) {
                    lastRightDebounceTime = millis();
                  } 
                  
                  if ((millis() - lastRightDebounceTime) > debounceDelay) {
                    buttonRightState = reading;
                   lastRightDebounceTime =millis();
                  }
                  
                  lastButtonRightState = reading;                  
                  reading = digitalRead(buttonPinLeft);
                  
                  if (reading != lastButtonLeftState) {
                    lastLeftDebounceTime = millis();
                  } 
        
                  if ((millis() - lastLeftDebounceTime) > debounceDelay) {
                    buttonLeftState = reading;
                    lastLeftDebounceTime=millis();;
                  }
                  lastButtonLeftState = reading;  

                  if (buttonEnterState == LOW){
                    lastButtonPushed=buttonPinEnter;
                  }else if(buttonEscState == LOW){                   
                    lastButtonPushed=buttonPinEsc;
                  }else if(buttonRightState == LOW){
                    lastButtonPushed=buttonPinRight;
                  }else if(buttonLeftState == LOW){
                    lastButtonPushed=buttonPinLeft;
                  }else{
                    lastButtonPushed=0;
                  }                  
}

void navigateMenus() {
  MenuItem currentMenu=menu.getCurrent();
  
  switch (lastButtonPushed){
    case buttonPinEnter:
      if(!(currentMenu.moveDown())){
        if(EEPROM.read(3) == 1){
          soundActive;
        }else{
          noSound;
        }  
        menu.use();
      }else{
        if(EEPROM.read(3) == 1){
          soundActive;
        }else{
          noSound;
        }  
        menu.moveDown();
       } 
      break;
    case buttonPinEsc:
      menu.toRoot();  
      break;
    case buttonPinRight:
      menu.moveRight();
      break;      
    case buttonPinLeft:
      menu.moveLeft();
      break;      
  }
  lastButtonPushed=0; 
}

void initialise_features(){
  Serial.print("Starting...");  
  if(EEPROM.read(3) == 1){
    soundActive;
  }else{
    noSound;
  }
  lcd.begin(16, 2);
//  lcd.print("SloMoments Robot");
//  lcd.setCursor(0, 1);
//  lcd.print("Controller v0.9");
}

void read_pins(){
}

void eeprom(){
  for (int i = 0; i < 40 ; i++)  
    EEPROM.write(i, 0);
}

void initialiseMenuButtons(){
  pinMode(buttonPinLeft,  INPUT_PULLUP);
  pinMode(buttonPinRight, INPUT_PULLUP);
  pinMode(buttonPinEnter, INPUT_PULLUP);
  pinMode(buttonPinEsc,   INPUT_PULLUP);
}


void confMenu(){
  menu.getRoot().add(miAnalog);
  miAnalog.addRight(miDigital).addRight(miSound);
  miAnalog.add(miASens1).addRight(miASens2);
  miDigital.add(miDSens1).addRight(miDSens2);
  miSound.add(miOn).addRight(miOff);
  menu.toRoot();
  lcd.setCursor(0,0);  
  lcd.print("SETTINGS        ");
}

void shortKeyPress() {
}

void longKeyPress() {
  if(EEPROM.read(3) == 1){
    soundActive;
  }else{
    noSound;
  }  
  confMenu(); 
}

void keyPress() {
    longKeyPressCount = 0;
}

void keyRelease() {    
    if (longKeyPressCount >= longKeyPressCountMax) {
        longKeyPress();
    }else {
        shortKeyPress();
    }
}

void shortkeyPress1() {
}

void  longkeyPress1() {
    initialise_features();
}

void keyPress1() {
    longkeyPressCount1 = 0;
}

void keyRelease1() {    
    if (longkeyPressCount1 >= longkeyPressCount1Max1) {
        longkeyPress1();
    }else {
        shortkeyPress1();
    }
}

char ceva(){
  if (millis() - keyPrevMillis >= keySampleIntervalMs) {
        keyPrevMillis = millis();
        
        byte currKeyState = digitalRead(keyPin);
        
        if ((prevKeyState == HIGH) && (currKeyState == LOW)) {
            keyPress();
        }
        else if ((prevKeyState == LOW) && (currKeyState == HIGH)) {
            keyRelease();
        }
        else if (currKeyState == LOW) {
            longKeyPressCount++;
        }
            
        prevKeyState = currKeyState;
    }
}

void ceva1(){
  if (millis() - keyPrevMillis1 >= keySampleIntervalMs1) {
        keyPrevMillis1 = millis();
        
        byte currKeyState1 = digitalRead(keyPin1);
        
        if ((prevKeyState1 == HIGH) && (currKeyState1 == LOW)) {
            keyPress1();
        }
        else if ((prevKeyState1 == LOW) && (currKeyState1 == HIGH)) {
            keyRelease1();
        }
        else if (currKeyState1 == LOW) {
            longkeyPressCount1++;
        }
        prevKeyState1 = currKeyState1;
    }
}

//unsigned thisReading[] = {0,0,0,0,0,0,0,0};
//void fors(){
//  for (int thisReadingF1 = 0; thisReadingF1 < 10; thisReadingF1++)
//    readingsF1[thisReadingF1] = 0;
//    
//    
//    
//    
//  for (int thisReadingF2 = 0; thisReadingF2 < numReadingsF2; thisReadingF2++)
//    readingsF2[thisReadingF2] = 0;
//  for (int thisReadingF3 = 0; thisReadingF3 < numReadingsF3; thisReadingF3++)
//    readingsF3[thisReadingF3] = 0;
//  for (int thisReadingF4 = 0; thisReadingF4 < numReadingsF4; thisReadingF4++)
//    readingsF4[thisReadingF4] = 0;
//  for (int thisReadingF5 = 0; thisReadingF5 < numReadingsF5; thisReadingF5++)
//    readingsF5[thisReadingF5] = 0;
//  for (int thisReadingF6 = 0; thisReadingF6 < numReadingsF6; thisReadingF6++)
//    readingsF6[thisReadingF6] = 0;
//  for (int thisReadingF7 = 0; thisReadingF7 < numReadingsF7; thisReadingF7++)
//    readingsF7[thisReadingF7] = 0;
//  for (int thisReadingF8 = 0; thisReadingF8 < numReadingsF8; thisReadingF8++)
//    readingsF8[thisReadingF8] = 0;
//}

void setup(){
  Serial.begin(9600);
  initialise_features();
  initialiseAnalogTune();
  initialiseEStop();
  initialiseMenuButtons();
  initialiseAnalogDetection();
  //fors();
}

void loop(){
  if(emergencyStop() == '0'){
  //  read_pins();
  //  analogTune();
  //  noSound;
  //  ceva();
    analogTune();
  //  readButtons();
  //  navigateMenus();
    //ceva1();  
    //processAnalogTune();
   // AnalogDetection();
  }else{
     alarm();
  }
}
