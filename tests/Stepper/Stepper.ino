// ConstantSpeed.pde
// -*- mode: C++ -*-
//
// Shows how to run AccelStepper in the simplest,
// fixed speed mode with no accelerations
/// \author  Mike McCauley (mikem@open.com.au)
// Copyright (C) 2009 Mike McCauley
// $Id: HRFMessage.h,v 1.1 2009/08/15 05:32:58 mikem Exp mikem $

#include <AccelStepper.h>

AccelStepper stepper(2, 22, 23); // Defaults to 4 pins on 2, 3, 4, 5

void setup()
{  
  stepper.setMinPulseWidth(100);
  stepper.setMaxSpeed(20000.0);
  stepper.setAcceleration(20000.0);
  stepper.setSpeed(20000);
  stepper.setCurrentPosition(0);
  stepper.moveTo(600);	
}

void loop()
{  
  stepper.run();
}
