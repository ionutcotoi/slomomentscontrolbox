#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>
#include <SerialEvent.h>
#include <PID_v1.h>
#include <SM.h>
#include "PID_AutoTune_v0.h"

int MOTOR1_PIN1 = 20;
int MOTOR1_PIN2 = 21;
int MOTOR2_PIN1 = 22;
int MOTOR2_PIN2 = 23;

int POT1 = 14;
int POT2 = 15;

int encoder1PinA = 2; // Pulse 
int encoder1PinB = 3; // Direction
int encoder2PinA = 4; // Pulse 
int encoder2PinB = 6; // Direction

int n1 = LOW;
long encoder1Pos = 0;
long encoder1Start = 0;
long encoder1End = 0;

int n2 = LOW;
long encoder2Pos = 0;
long encoder2Start = 0;
long encoder2End = 0;

int analogInput1Val = 0;
int analogInput2Val = 0;

#define NUM_SAME_VALS 5

#define DOWN  -1
#define UP     1

#define CALIB_SPEED  30  
#define RUN_SPEED    255

#define MAX_FOCUS_VALUE 10000
#define DEVICE_LEN 2

int direction1 = DOWN; // - , UP +
int direction2 = DOWN; // - , UP +

Encoder myEnc1(encoder1PinA, encoder1PinB);
Encoder myEnc2(encoder2PinA, encoder2PinB);

double currentPos1=0;
double targetPos1=0;
double spd1=0;
double currentPos2=0;
double targetPos2=0;
double spd2=0;

double Kp = 0.3;
double Ki = 0.5;
double Kd = 0.01;

PID FocusPID1(&currentPos1, &spd1, &targetPos1,Kp,Ki,Kd, DIRECT);
PID FocusPID2(&currentPos2, &spd2, &targetPos2,Kp,Ki,Kd, DIRECT);


SM Focus1(CalibStart1);
SM Focus2(CalibStart2);

String inputString = "";
boolean stringComplete = false;

void setup() {
  pinMode(POT1, INPUT);
  pinMode(POT2, INPUT);
  pinMode(MOTOR1_PIN1, OUTPUT);
  pinMode(MOTOR1_PIN2, OUTPUT);
  pinMode(MOTOR2_PIN1, OUTPUT);
  pinMode(MOTOR2_PIN2, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(15, OUTPUT);
  
  Serial2.begin(115200);
  inputString.reserve(30);
  //Serial2.println("Serial2 on");
  Serial.begin (115200);
  digitalWrite(11, LOW);
  digitalWrite(12, LOW);
  digitalWrite(13, HIGH);
  digitalWrite(15, HIGH);

  FocusPID1.SetMode(AUTOMATIC);
  FocusPID1.SetOutputLimits(-RUN_SPEED,RUN_SPEED);
  FocusPID1.SetSampleTime(1);
  FocusPID2.SetMode(AUTOMATIC);
  FocusPID2.SetOutputLimits(-RUN_SPEED,RUN_SPEED);
  FocusPID2.SetSampleTime(1);
  delay(5000);
}

void loop() {
  EXEC(Focus1);
  //EXEC(Focus2);
  if(stringComplete) {
      SerialCom();
      stringComplete = false;
    }
  Serial2Event();
}

void motor1(int speedLeft) {
  if (speedLeft > 0) {
    direction1 = UP;
    analogWrite(MOTOR1_PIN1, speedLeft);
    analogWrite(MOTOR1_PIN2, 0);
  } 
  else {
    direction1 = DOWN;
    analogWrite(MOTOR1_PIN1, 0);
    analogWrite(MOTOR1_PIN2, -speedLeft);
  }
}
void motor2(int speedLeft) {
  if (speedLeft > 0) {
    direction2 = UP;
    analogWrite(MOTOR2_PIN1, speedLeft);
    analogWrite(MOTOR2_PIN2, 0);
  } 
  else {
    direction2 = DOWN;
    analogWrite(MOTOR2_PIN1, 0);
    analogWrite(MOTOR2_PIN2, -speedLeft);
  }
}

// This is not used anymore, is just for reference
/*
void go(int speedLeft, int speedRight) {
  if (speedLeft > 0) {
    analogWrite(MOTOR1_PIN1, speedLeft);
    analogWrite(MOTOR1_PIN2, 0);
  } 
  else {
    analogWrite(MOTOR1_PIN1, 0);
    analogWrite(MOTOR1_PIN2, -speedLeft);
  }
 
  if (speedRight > 0) {
    analogWrite(MOTOR2_PIN1, speedRight);
    analogWrite(MOTOR2_PIN2, 0);
  }else {
    analogWrite(MOTOR2_PIN1, 0);
    analogWrite(MOTOR2_PIN2, -speedRight);
  }
}
*/

State CalibStart1(){
  static int sameValCount = 0;
  motor1(CALIB_SPEED);
  delay(100);
  long newPosition = myEnc1.read();
  
  if(newPosition != encoder1Pos) {
    encoder1Pos = newPosition;
  } else {
    sameValCount++;
  }
  
  if(sameValCount > NUM_SAME_VALS) {
    Serial.println("F0: Am ajuns la start");
    encoder1Start = encoder1Pos;
    motor1(0);
    delay(1000);
    Focus1.Set(CalibEnd1);
  }
}

State CalibStart2(){
  static int sameValCount = 0;
  motor2(CALIB_SPEED);
  delay(100);
  
  long newPosition = myEnc2.read();
  
  if(newPosition != encoder2Pos) {
    encoder2Pos = newPosition;
    Serial.println(newPosition, DEC);
  } else {
    sameValCount++;
  }
  
  if(sameValCount > NUM_SAME_VALS) {
    Serial.println("F1: Am ajuns la start");
    encoder2Start = encoder2Pos;
    motor2(0);
    delay(1000);
    Focus2.Set(CalibEnd2);
  }
}


State CalibEnd1(){
  static long sameValCount = 0;
  motor1(-CALIB_SPEED);
  delay(100);
  
  long newPosition = myEnc1.read();
  if(newPosition != encoder1Pos) {
    encoder1Pos = newPosition;
    Serial.println(newPosition, DEC);
  } else {
    sameValCount++;
  }
  
  if(sameValCount > NUM_SAME_VALS) {
    Serial.println("F0: Am ajuns la final");
    encoder1End = encoder1Pos;
    motor1(0);
    delay(1000);
    targetPos1 = double(map(MAX_FOCUS_VALUE/2, 0, MAX_FOCUS_VALUE, encoder1Start, encoder1End));
    Focus1.Set(PrintVals1);
  }
}

State CalibEnd2(){
  static long sameValCount = 0;
  motor2(-CALIB_SPEED);
  delay(100);
  
  long newPosition = myEnc2.read();
  if(newPosition != encoder2Pos) {
    encoder2Pos = newPosition;
    //Serial.println(newPosition, DEC);
  } else {
    sameValCount++;
  }
  
  if(sameValCount > NUM_SAME_VALS) {
    //Serial.println("Am ajuns la final");
    encoder2End = encoder2Pos;
    motor2(0);
    delay(1000);
    Focus2.Set(PrintVals2);
  }
}

State PrintVals1() {
  Serial.println("MOTOR 1:");
  Serial.print("START: ");
  Serial.println(encoder1Start, DEC);
  Serial.print("END:");
  Serial.println(encoder1End, DEC);
  Serial.println("TARGET  POSITION");
  Focus1.Set(Running1);
}

State PrintVals2() {
  Serial.println("MOTOR 2:");
  Serial.print("START: ");
  Serial.println(encoder2Start, DEC);
  Serial.print("END:");
  Serial.println(encoder2End, DEC);
  Serial.println("TARGET  POSITION");
  Focus2.Set(Running2);
}

State PID_tune1(){
  PID_ATune aTune1(&currentPos1, &spd1);
  targetPos1 = double(map(MAX_FOCUS_VALUE/2, 0, MAX_FOCUS_VALUE, encoder1Start, encoder1End));
  double epsilon = abs(encoder1Start-encoder1End)/MAX_FOCUS_VALUE;
  currentPos1 = double(myEnc1.read());
  while(abs(targetPos1-currentPos1)>epsilon){
    FocusPID1.Compute();
    motor1(spd1);
    currentPos1 = double(myEnc1.read());
  }
  motor1(0);
  delay(500);
  aTune1.SetOutputStep(RUN_SPEED);
  aTune1.SetControlType(1);
  aTune1.SetNoiseBand(epsilon);
  aTune1.SetLookbackSec(200);
  while(aTune1.Runtime() == 0){
    currentPos1 = double(myEnc1.read());
    motor1((int)spd1);
  }
  double kp = aTune1.GetKp();
  double ki = aTune1.GetKi();
  double kd = aTune1.GetKd();
  if (kp > 0.5){
    FocusPID1.SetTunings(kp,ki,kd);
  }
  Focus1.Set(Running1);
}

State PID_tune2(){
  PID_ATune aTune2(&currentPos2, &spd2);
  Focus2.Set(Running2);
}

State Running1() {
  // int analogVal1 = analogRead(POT1)/4; //analogRead returns a value between 0, 1023, we map it in 0,255
  //   //Serial.println(analogInput1Val, DEC);
  //   double lpf_size = 100;
  //   double p = 1/lpf_size;
  //   targetPos1 = (1-p)*targetPos1+p*double(map(analogVal1, 0, 255, encoder1Start, encoder1End));
  //   Serial.print(analogVal1, DEC);
  //   Serial.print("\t");
    char tmp[100];
    
    
    currentPos1 = double(myEnc1.read());
    FocusPID1.Compute();
    
    motor1((int)spd1);
    sprintf(tmp, "%lf\t%lf\t%d\n", targetPos1, currentPos1, (int)spd1);
    Serial.print(tmp);
}

State Running2() {
  // int analogVal2 = analogRead(POT2)/4;
  // double lpf_size = 500;
  //   //Serial.println(analogInput1Val, DEC);
  // double p = 1/lpf_size;
  //   targetPos2 = (1-p)*targetPos2 + p*double(map(analogVal2, 0, 255, encoder2Start, encoder2End));
  //   Serial.print(analogVal2, DEC);
  //   Serial.print("\t");
  //   Serial.print(targetPos2, DEC);
  //   Serial.print("\t");
  //   Serial.println(currentPos2, DEC);
    
    
    currentPos2 = double(myEnc2.read());

    FocusPID2.Compute();

    motor2(spd2);
}
void SerialCom(){
  // update targetPos1 or targetPos2
  char charBuf[50];
  inputString.toCharArray(charBuf, 50) ;
  char ioType = charBuf[0];
  int ioNum  = atoi(charBuf+1);
  int value = constrain(atoi(charBuf+DEVICE_LEN+1),0,MAX_FOCUS_VALUE);
  if (ioType=='F'){
    if (ioNum == 0){
      //Serial2.print("F0 command received\nvalue= ");
      //Serial2.println(value);
      targetPos1 = double(map(value, 0, MAX_FOCUS_VALUE, encoder1Start, encoder1End));
      //Serial2.print("target pos F0: ");
      //Serial2.println(targetPos1);
    }
    else if (ioNum==1){
      //Serial2.print("F1 command received\nvalue= ");
      //Serial2.println(value);
      targetPos2 = double(map(value, 0, MAX_FOCUS_VALUE, encoder2Start, encoder2End));
    }
    
  }
  inputString = "";
}
void Serial2Event() {
  while (Serial2.available()) {
    // get the new byte:
    char inChar = (char)Serial2.read(); 
    //Serial.print(inChar);
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if ((inChar == '\n') || (inChar == '\r') || (inChar == '\\')) {
      stringComplete = true;
    } 
  }
}
