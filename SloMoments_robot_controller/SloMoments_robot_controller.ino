/* SloMoments Robot controller rev 1.0 */
//this is the program
#include "Wire.h"
#include <LiquidCrystal.h>
#include <EEPROM.h>
#include <MenuBackend.h>

LiquidCrystal lcd(0);

unsigned analogSensorsTotal = 16;
unsigned analogIn[] = {A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14, A15};
unsigned analogVal[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned digitalIn[] = {22, 24, 26, 28, 30, 32, 34, 36};
unsigned digitalOut[] = {23, 25, 27, 29, 31, 33, 35, 37};
unsigned digitalVal[] = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned keypadIn[] = {2, 3, 4, 5};

int sensorPin1 = A0; 
int sensorPin2 = A1; 
int sensorPin3 = A2; 
int sensorPin4 = A3; 
int sensorPin5 = A4; 
int sensorPin6 = A5; 
int sensorPin7 = A6; 
int sensorPin8 = A7;

int sensor1 = 0; 
int sensor2 = 0; 
int sensor3 = 0; 
int sensor4 = 0; 
int sensor5 = 0; 
int sensor6 = 0; 
int sensor7 = 0; 
int sensor8 = 0;

#define ESC_PIN    2
#define KB_PIN     3
#define DOWN_PIN   4
#define UP_PIN     5
#define pinEstop   7
#define pinBuzzer 12
#define txSerial  18
#define rxSerial  19
#define sdaLcd    20
#define sclLcd    21

#define soundActive tone(pinBuzzer, 2000, 200)
#define noSound noTone(pinBuzzer)

int lastButtonPushed = 0;

int lastButtonEnterState = LOW;  
int lastButtonEscState = LOW;   
int lastButtonLeftState = LOW;   
int lastButtonRightState = LOW;   

long lastEnterDebounceTime = 0;  
long lastEscDebounceTime = 0;  
long lastLeftDebounceTime = 0;  
long lastRightDebounceTime = 0;  
long debounceDelay = 100;

bool menurefreshed = true;

MenuBackend menu = MenuBackend(menuUsed,menuChanged);

      MenuItem miAnalog = MenuItem("Analog sens");
            MenuItem miASens1 = MenuItem(">");
            MenuItem miASens2 = MenuItem("<");
           
      MenuItem miDigital = MenuItem("Digital sens");
            MenuItem miDSens1 = MenuItem(">");
            MenuItem miDSens2 = MenuItem("<");

      MenuItem miSound = MenuItem("Sound");
            MenuItem miOn = MenuItem("on");
            MenuItem miOff = MenuItem("off");

int totalF1 = 0;
int readingsF1[10];
int indexF1 = 0;
int averageF1 = 0;
int valoriF1[100];
int indexvalF1 = 0;
int val_ant1;

void setup(){
  initialise_pins();
  initialise_features();
  //initialiseAnalogTune();
  for (int thisReadingF1 = 0; thisReadingF1 < 10; thisReadingF1++)
    readingsF1[thisReadingF1] = 0;
  
}

void loop(){
  if(emergencyStop() == '0'){
    //readPins();
    //display_refresh();
    //serial_write();
    readButtons(); 
    navigateMenus();    
    noTone(pinBuzzer);
    processAnalogInputs();
    if (!menurefreshed){
      lcd_default();
    }
  }else{
     alarm();
  }
}

void initialise_pins(){
  pinMode(A0, INPUT_PULLUP);
  pinMode(A1, INPUT_PULLUP);
  pinMode(A2, INPUT_PULLUP);
  pinMode(A3, INPUT_PULLUP);
  pinMode(A4, INPUT_PULLUP);
  pinMode(A5, INPUT_PULLUP);
  pinMode(A6, INPUT_PULLUP);
  pinMode(A7, INPUT_PULLUP);
  pinMode(A8, INPUT_PULLUP);
  pinMode(A9, INPUT_PULLUP);
  pinMode(A10, INPUT_PULLUP);
  pinMode(A11, INPUT_PULLUP);
  pinMode(A12, INPUT_PULLUP);
  pinMode(A13, INPUT_PULLUP);
  pinMode(A14, INPUT_PULLUP);
  pinMode(A15, INPUT_PULLUP);
  
  pinMode(UP_PIN, INPUT_PULLUP);
  pinMode(DOWN_PIN, INPUT_PULLUP);
  pinMode(KB_PIN, INPUT_PULLUP);
  pinMode(ESC_PIN, INPUT_PULLUP);
}

void initialise_features(){
  //Serial communication with the laptop
  Serial.begin(9600);
  Serial.println("Starting...");
  //tone start
  tone(pinBuzzer, 2000, 200);
  // set up the LCD's number of columns and rows: 
  //LCD init
  lcd.begin(16, 2);
  //EStop init
  initialiseEStop();
  //Initialise Menu
  menu.getRoot().add(miAnalog);
  miAnalog.addRight(miDigital).addRight(miSound);
  miAnalog.add(miASens1).addRight(miASens2);
  miDigital.add(miDSens1).addRight(miDSens2);
  miSound.add(miOn).addRight(miOff);
  menu.toRoot();
  lcd_default();
}

void readPins(){
  for(int i=0; i<=analogSensorsTotal; i++){
    //analogVal[i] = analogRead(analogIn[i]);
    //Serial.println(analogVal[i]);
  }
  //Serial.println(digitalRead(7));
  if(analogRead(A2)<20){
    //lcd.print("Laser triggered");
    //Serial.println("Laser triggered");
  }else{
    //lcd.print("Ready!");
    //Serial.println("ready");
  }
}

void display_refresh(){

}

void serial_write(){

}



void menuChanged(MenuChangeEvent changed){
  
  MenuItem newMenuItem=changed.to; //get the destination menu
  
  lcd.setCursor(0,0); 
  
  if(newMenuItem.getName()==menu.getRoot()){
      lcd_default();
  }else if(newMenuItem.getName()=="Analog sens"){
      lcd.print("  Analog sens   ");
  }else if(newMenuItem.getName()==">"){
      lcd.print("       ->       ");
  }else if(newMenuItem.getName()=="<"){
      lcd.print("       <-       ");
  }else if(newMenuItem.getName()=="Digital sens"){
      lcd.print("  Digital Sens  ");
  }else if(newMenuItem.getName()==">"){
      lcd.print("       ->       ");
  }else if(newMenuItem.getName()=="<"){
      lcd.print("       <-       ");
  }else if(newMenuItem.getName()=="Sound"){
      lcd.print("     Sound      ");
  }else if(newMenuItem.getName()=="on"){
      lcd.print("       on       ");
      EEPROM.write(3,1);
  }else if(newMenuItem.getName()=="off"){
      lcd.print("       off      ");
      EEPROM.write(3,0);
  }
  
}

void menuUsed(MenuUseEvent used){
  lcd.setCursor(0,0);  
  lcd.print("You used        ");
  //lcd.setCursor(0,1); 
  //lcd.print(used.item.getName());
  delay(3000);  
//  lcd.setCursor(0,0);  
//  lcd.print("Settings        ");
  menu.toRoot();  
  lcd_default();
}


void  readButtons(){  //read buttons status
  int reading;
  int buttonEnterState=HIGH;             
  int buttonEscState=HIGH;            
  int buttonLeftState=HIGH;           
  int buttonRightState=HIGH;          
  
  reading = digitalRead(KB_PIN);
  if (reading != lastButtonEnterState) {
    lastEnterDebounceTime = millis();
  } 
  if ((millis() - lastEnterDebounceTime) > debounceDelay) {
    buttonEnterState=reading;
    lastEnterDebounceTime=millis();
  }
  lastButtonEnterState = reading;
  reading = digitalRead(ESC_PIN);
  
 if (reading != lastButtonEscState) {
    lastEscDebounceTime = millis();
  } 
  
  if ((millis() - lastEscDebounceTime) > debounceDelay) {
    buttonEscState = reading;
    lastEscDebounceTime=millis();
  }
  lastButtonEscState = reading; 
  reading = digitalRead(DOWN_PIN);
  if (reading != lastButtonRightState) {
    lastRightDebounceTime = millis();
  } 
  
  if ((millis() - lastRightDebounceTime) > debounceDelay) {
    buttonRightState = reading;
   lastRightDebounceTime =millis();
  }
  lastButtonRightState = reading;                  
  reading = digitalRead(UP_PIN);
  if (reading != lastButtonLeftState) {
    lastLeftDebounceTime = millis();
  } 
  
  if ((millis() - lastLeftDebounceTime) > debounceDelay) {
    buttonLeftState = reading;
    lastLeftDebounceTime=millis();;
  }
  lastButtonLeftState = reading;  

  if (buttonEnterState==LOW){
    lastButtonPushed=KB_PIN;

  }else if(buttonEscState==LOW){
    lastButtonPushed=ESC_PIN;

  }else if(buttonRightState==LOW){
    lastButtonPushed=DOWN_PIN;

  }else if(buttonLeftState==LOW){
    lastButtonPushed=UP_PIN;

  }else{
    lastButtonPushed=0;
  }                  
}

void navigateMenus() {
  MenuItem currentMenu=menu.getCurrent();
  
  switch (lastButtonPushed){
    case KB_PIN:
      if(!(currentMenu.moveDown())){  //if the current menu has a child and has been pressed enter then menu navigate to item below
        menu.use();
      }else{  //otherwise, if menu has no child and has been pressed enter the current menu is used
        menu.moveDown();
       } 
      break;
    case ESC_PIN:
      menu.toRoot();  
      //TO DO JUMP ONE LEVEL UP menu.moveUp();
      break;
    case DOWN_PIN:
      menu.moveRight();
      break;      
    case UP_PIN:
      menu.moveLeft();
      break;      
  }
  
  lastButtonPushed=0; 
}

void lcd_default(){
  lcd.setCursor(0,0);  
  lcd.print("SloMoments Robo1");
  menurefreshed = true;
  lcd.setCursor(0,1);
    lcd.print("      ");
}
