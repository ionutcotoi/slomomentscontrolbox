boolean estop = false;

void initialiseEStop(){
  pinMode(pinEstop, INPUT_PULLUP);
}

char emergencyStop(){
  if(digitalRead(pinEstop)){
    estop = false;
    return '0';
  }else{
    estop = true;
    return '1';
  }
}

void alarm(){
  lcd.setCursor(0,0);
  lcd.print(" EMERGENCY STOP ");
  tone(pinBuzzer, 5000);
  delay(50);
  tone(pinBuzzer, 1000);
  delay(50);
  menurefreshed = false;
}
